package uca.fastnotes.api.models;

import io.realm.RealmObject;

public class Notes extends RealmObject {
    private int idnotes;
    private String title;
    private String description;
    private int iduser;
    private String created_at;
    private String updated_at;

    public int getIdnotes() {
        return idnotes;
    }
    public void setIdnotes(int idnotes) {
        this.idnotes = idnotes;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getIduser() {
        return iduser;
    }
    public void setIduser(int iduser) {
        this.iduser = iduser;
    }
    public String getCreated_at() {
        return created_at;
    }
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
    public String getUpdated_at() {
        return updated_at;
    }
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
