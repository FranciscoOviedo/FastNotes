package uca.fastnotes.api.models;

import io.realm.RealmObject;

public class Assignments extends RealmObject {
    private int idassignment;
    private String name;
    private boolean state;
    private int iduser;
    private String created_at;
    private String updated_at;

    public int getIdassignment() {
        return idassignment;
    }

    public void setIdassignment(int idassignment) {
        this.idassignment = idassignment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }
}
