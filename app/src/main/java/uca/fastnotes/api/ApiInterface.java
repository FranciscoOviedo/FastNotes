package uca.fastnotes.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import uca.fastnotes.api.models.Assignments;
import uca.fastnotes.api.models.Notes;
import uca.fastnotes.api.models.User;

public interface ApiInterface {
    @GET("/assignments")
    Call<List<Assignments>> listAssignment();
    @GET("/assignments/byuser/{id}")
    Call<List<Assignments>> listAssignmentBy(@Path("id") int userid);
    @POST("/assignments/create")
    Call<Assignments> createAssignment(@Body Assignments assignment);
    @PUT("/assignments/update/{id}")
    Call<Assignments> updateAssignment(@Path("id") String id, @Body Assignments assignment);
    @DELETE("assignments/delete/{id}")
    Call<Assignments> deleteAssignment(@Path("id") String id);

    @GET("/notes")
    Call<List<Notes>> listNote();
    @GET("/notes/byuser/{id}")
    Call<List<Notes>> listNoteBy(@Path("id") int userid);
    @POST("/notes/create")
    Call<Notes> createNote(@Body Notes notes);
    @PUT("/notes/update/{id}")
    Call<Notes> updateNote(@Path("id") String id, @Body Notes notes);
    @DELETE("notes/delete/{id}")
    Call<Notes> deleteNote(@Path("id") String id);

    @GET("/activities")
    Call<List<Assignments>> listActivity();
    @GET("/activities/byuser/{id}")
    Call<List<Assignments>> listActivityBy(@Path("id") int userid);
    @POST("/activities/create")
    Call<Assignments> createActivity(@Body Assignments assignment);
    @PUT("/activities/update/{id}")
    Call<Assignments> updateActivity(@Path("id") String id, @Body Assignments assignment);
    @DELETE("activities/delete/{id}")
    Call<Assignments> deleteActivity(@Path("id") String id);

    @POST("/users/create")
    Call<User> createUser(@Body User user);
    @POST("/users/login")
    Call<User> loginUser(@Body User user);
}
