package uca.fastnotes.ui.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.tumblr.remember.Remember;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uca.fastnotes.R;
import uca.fastnotes.api.ApiFastNotes;
import uca.fastnotes.api.models.User;
import uca.fastnotes.ui.TabsView;

public class EnterUser extends AppCompatActivity {
    private TextView et_email;
    private TextView et_password;
    private Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_user);
        getSupportActionBar().hide();
        initView();
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User tempUser = new User();
                tempUser.setEmail(et_email.getText().toString());
                tempUser.setPassword(et_password.getText().toString());
                Call<User> userCall = ApiFastNotes.instance().loginUser(tempUser);
                final View finalView = view;
                userCall.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        User responseUser = response.body();
                        if(responseUser.getIduser()==-1){
                            new MaterialDialog.Builder(finalView.getContext())
                                    .title(R.string.incorrect_user_title)
                                    .content(R.string.incorrect_user_description)
                                    .show();
                        } else {
                            storeUserID(responseUser.getIduser());
                            Intent intent = new Intent(finalView.getContext(), TabsView.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {

                    }
                });
            }
        });

    }

    private void initView(){
        et_email = (TextView) findViewById(R.id.et_email);
        et_password = (TextView) findViewById(R.id.et_password);
        btn_login = (Button) findViewById(R.id.btn_login);
    }

    private void storeUserID(int userid) {
        Remember.putInt(User.USER_ID, userid);
    }
}
