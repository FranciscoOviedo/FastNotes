package uca.fastnotes.ui.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.tumblr.remember.Remember;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uca.fastnotes.R;
import uca.fastnotes.api.ApiFastNotes;
import uca.fastnotes.api.models.User;
import uca.fastnotes.ui.TabsView;

public class CreateUser extends AppCompatActivity {
    private EditText et_username;
    private EditText et_email;
    private EditText et_password;
    private Button btn_register;
    private boolean waitingCreateResponse = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_create_user);
        initView();

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!waitingCreateResponse){
                    waitingCreateResponse=true;

                    User apiUser = new User();
                    apiUser.setUsername(et_username.getText().toString());
                    apiUser.setEmail(et_email.getText().toString());
                    apiUser.setPassword(et_password.getText().toString());
                    Call<User> userCall = ApiFastNotes.instance().createUser(apiUser);

                    final View finalView = view;
                    userCall.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            User responseUser = response.body();
                            storeUserID(responseUser.getIduser());
                            Intent intent = new Intent(finalView.getContext(), TabsView.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            waitingCreateResponse=false;
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            waitingCreateResponse=false;
                        }
                    });
                }
            }
        });
    }

    private void initView(){
        et_username = (EditText) findViewById(R.id.et_username);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_register = (Button) findViewById(R.id.btn_register);
    }

    private void storeUserID(int userid) {
        Remember.putInt(User.USER_ID, userid);
    }
}
