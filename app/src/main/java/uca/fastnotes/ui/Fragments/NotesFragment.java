package uca.fastnotes.ui.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tumblr.remember.Remember;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uca.fastnotes.R;
import uca.fastnotes.api.ApiFastNotes;
import uca.fastnotes.api.models.Notes;
import uca.fastnotes.api.models.User;
import uca.fastnotes.ui.adapters.ItemClickListener;
import uca.fastnotes.ui.adapters.NotesAdapter;
import uca.fastnotes.ui.assignments.EditAssignmentActivity;

public class NotesFragment extends Fragment {
    private static final String IS_FIRST_TIME_NOTES = "is_first_time_notes";
    private View rootView;

    private RecyclerView rv_notes;

    private NotesAdapter mAssignmentAdapter;
    private List<Notes> mItemList = new ArrayList<>();
    private SwipeRefreshLayout srl_notes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_notes, container, false);
        initView();
        if (!isFirstTime()) {
            fetchNotes();
            storeFirstTime();
        } else {
            getFromDataBase();
        }

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        fetchNotes();
    }

    private void initView(){
        rv_notes = (RecyclerView) rootView.findViewById(R.id.rv_notes);
        srl_notes = (SwipeRefreshLayout) rootView.findViewById(R.id.srl_notes);

        rv_notes.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_notes.setLayoutManager(linearLayoutManager);
        mAssignmentAdapter = new NotesAdapter(mItemList);
        rv_notes.setAdapter(mAssignmentAdapter);

        srl_notes.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchNotes();
            }
        });
    }

    private void fetchNotes() {
        Call<List<Notes>> call = ApiFastNotes.instance().listNoteBy(getUserID());
        call.enqueue(new Callback<List<Notes>>() {
            @Override
            public void onResponse(Call<List<Notes>> call, Response<List<Notes>> response) {
                sync(response.body());
                getFromDataBase();
                /*mItemList.clear();
                mItemList.addAll(response.body());
                mAssignmentAdapter.notifyDataSetChanged();*/
                srl_notes.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Notes>> call, Throwable t) {
                srl_notes.setRefreshing(false);
                Snackbar.make(rootView, rootView.getContext().getString(R.string.no_internet_error), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void storeFirstTime() {
        Remember.putBoolean(IS_FIRST_TIME_NOTES, true);
    }

    private boolean isFirstTime() {
        return Remember.getBoolean(IS_FIRST_TIME_NOTES, false);
    }

    private void sync(List<Notes> noteModelList) {
        Realm realm = Realm.getDefaultInstance();

        RealmQuery<Notes> query = realm.where(Notes.class);
        RealmResults<Notes> results = query.findAll();

        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();

        for(Notes noteModel : noteModelList) {
            store(noteModel);
        }
    }

    private void store(Notes apiNoteModel) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();

        Notes realmNoteModel = realm.createObject(Notes.class); // Create a new object

        realmNoteModel.setIdnotes(apiNoteModel.getIdnotes());
        realmNoteModel.setTitle(apiNoteModel.getTitle());
        realmNoteModel.setDescription(apiNoteModel.getDescription());
        realmNoteModel.setCreated_at(apiNoteModel.getCreated_at());
        realmNoteModel.setUpdated_at(apiNoteModel.getUpdated_at());
        realmNoteModel.setIduser(apiNoteModel.getIduser());

        realm.commitTransaction();
    }

    private void getFromDataBase() {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Notes> query = realm.where(Notes.class);

        RealmResults<Notes> results = query.findAll();

        mItemList.clear();
        mItemList.addAll(results);
        mAssignmentAdapter.notifyDataSetChanged();
    }

    private int getUserID() {
        return Remember.getInt(User.USER_ID, -7);
    }
}
