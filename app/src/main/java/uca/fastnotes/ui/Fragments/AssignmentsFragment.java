package uca.fastnotes.ui.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tumblr.remember.Remember;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uca.fastnotes.R;
import uca.fastnotes.api.ApiFastNotes;
import uca.fastnotes.api.models.Assignments;
import uca.fastnotes.api.models.User;
import uca.fastnotes.ui.adapters.AssignmentsAdapter;
import uca.fastnotes.ui.adapters.ItemClickListener;
import uca.fastnotes.ui.assignments.EditAssignmentActivity;

public class AssignmentsFragment extends Fragment {
    private static final String IS_FIRST_TIME_ASSIGNMENTS = "is_first_time_assignments";
    private View rootView;

    private RecyclerView rv_assignment;

    private AssignmentsAdapter mAssignmentAdapter;
    private List<Assignments> mItemList = new ArrayList<>();
    private SwipeRefreshLayout srl_assignment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_assignments, container, false);
        initView();
        if (!isFirstTime()) {
            fetchAssignments();
            storeFirstTime();
        } else {
            getFromDataBase();
        }

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        fetchAssignments();
    }

    private void initView(){
        rv_assignment = (RecyclerView) rootView.findViewById(R.id.rv_assignment);
        srl_assignment = (SwipeRefreshLayout) rootView.findViewById(R.id.srl_assignment);

        rv_assignment.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_assignment.setLayoutManager(linearLayoutManager);
        final Activity temp = getActivity();
        mAssignmentAdapter = new AssignmentsAdapter(mItemList);
        rv_assignment.setAdapter(mAssignmentAdapter);

        srl_assignment.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchAssignments();
            }
        });
    }

    private void fetchAssignments() {
        Call<List<Assignments>> call = ApiFastNotes.instance().listAssignmentBy(getUserID());
        call.enqueue(new Callback<List<Assignments>>() {
            @Override
            public void onResponse(Call<List<Assignments>> call, Response<List<Assignments>> response) {
                sync(response.body());
                getFromDataBase();
                srl_assignment.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Assignments>> call, Throwable t) {
                srl_assignment.setRefreshing(false);
                Snackbar.make(rootView, rootView.getContext().getString(R.string.no_internet_error), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void storeFirstTime() {
        Remember.putBoolean(IS_FIRST_TIME_ASSIGNMENTS, true);
    }

    private boolean isFirstTime() {
        return Remember.getBoolean(IS_FIRST_TIME_ASSIGNMENTS, false);
    }

    private void sync(List<Assignments> assignmentModelList) {
        Realm realm = Realm.getDefaultInstance();

        RealmQuery<Assignments> query = realm.where(Assignments.class);
        RealmResults<Assignments> results = query.findAll();

        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();

        for(Assignments assignmentModel : assignmentModelList) {
            store(assignmentModel);
        }
    }

    private void store(Assignments apiassignmentModel) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();

        Assignments realmAssignmentModel = realm.createObject(Assignments.class); // Create a new object


        realmAssignmentModel.setIdassignment(apiassignmentModel.getIdassignment());
        realmAssignmentModel.setName(apiassignmentModel.getName());
        realmAssignmentModel.setState(apiassignmentModel.isState());
        realmAssignmentModel.setCreated_at(apiassignmentModel.getCreated_at());
        realmAssignmentModel.setUpdated_at(apiassignmentModel.getUpdated_at());
        realmAssignmentModel.setIduser(apiassignmentModel.getIduser());

        realm.commitTransaction();
    }

    private void getFromDataBase() {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Assignments> query = realm.where(Assignments.class);

        RealmResults<Assignments> results = query.findAll();

        mItemList.clear();
        mItemList.addAll(results);
        mAssignmentAdapter.notifyDataSetChanged();
    }

    private int getUserID() {
        return Remember.getInt(User.USER_ID, -7);
    }
}
