package uca.fastnotes.ui;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.tumblr.remember.Remember;

import uca.fastnotes.R;
import uca.fastnotes.api.models.User;
import uca.fastnotes.ui.adapters.SectionPagerAdapter;
import uca.fastnotes.ui.assignments.CreateAssignmentActivity;
import uca.fastnotes.ui.notes.CreateNotesActivity;

public class TabsView extends AppCompatActivity{
    private SectionPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private Context mContext;
    private FloatingActionButton mFabAdd;
    private int currentTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs_view);

        mContext = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.containerPager);
        mFabAdd = (FloatingActionButton) findViewById(R.id.fabAdd);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        //mFabAdd.setOnClickListener(this);
        currentTab = 0;
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentTab = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        mFabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentTab==0){
                    Intent i = new Intent(v.getContext(), CreateAssignmentActivity.class);
                    startActivity(i);
                } else if(currentTab==1) {
                    Intent i = new Intent(v.getContext(), CreateNotesActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(v.getContext(), "Undefined Activity", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tabs_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_sign_out) {
            storeUserID(-7);
            Intent intent = new Intent(mContext, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void storeUserID(int userid) {
        Remember.putInt(User.USER_ID, userid);
    }
}
