package uca.fastnotes.ui.notes;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uca.fastnotes.R;
import uca.fastnotes.api.ApiFastNotes;
import uca.fastnotes.api.models.Notes;

public class EditNotesActivity extends AppCompatActivity {
    EditText et_title;
    EditText et_description;
    Button btn_edit;
    private ConstraintLayout mConstraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_notes);
        initView();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        final int notesID = intent.getIntExtra("NotesID", 0);
        String name = intent.getStringExtra("NotesTitle");
        String description = intent.getStringExtra("NotesDescription");
        et_title.setText(name);
        et_description.setText(description);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Notes tempNote = new Notes();
                tempNote.setTitle(et_title.getText().toString());
                tempNote.setDescription(et_description.getText().toString());
                Call<Notes> call = ApiFastNotes.instance().updateNote(String.valueOf(notesID), tempNote);
                call.enqueue(new Callback<Notes>() {
                    @Override
                    public void onResponse(Call<Notes> call, Response<Notes> response) {
                        finish();
                    }

                    @Override
                    public void onFailure(Call<Notes> call, Throwable t) {

                    }
                });
            }
        });
    }

    private void initView(){
        et_title = (EditText) findViewById(R.id.et_title);
        et_description = (EditText) findViewById(R.id.et_description);
        btn_edit = (Button) findViewById(R.id.btn_edit);
        mConstraintLayout = (ConstraintLayout) findViewById(R.id.constraintEditNotes);
    }

    @Override
    public boolean  onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id==android.R.id.home) {
            finish();
        }
        return true;
    }
}
