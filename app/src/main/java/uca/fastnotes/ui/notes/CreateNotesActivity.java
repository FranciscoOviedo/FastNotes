package uca.fastnotes.ui.notes;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tumblr.remember.Remember;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uca.fastnotes.R;
import uca.fastnotes.api.ApiFastNotes;
import uca.fastnotes.api.models.Notes;
import uca.fastnotes.api.models.User;

public class CreateNotesActivity extends AppCompatActivity {
    EditText et_title;
    EditText et_description;
    Button btn_save;
    private ConstraintLayout mConstraintLayout;
    private boolean waitingCreateResponse = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_notes);
        initView();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!waitingCreateResponse) {
                    waitingCreateResponse=true;

                    Notes tempNotes = new Notes();
                    tempNotes.setTitle(et_title.getText().toString());
                    tempNotes.setDescription(et_description.getText().toString());
                    tempNotes.setIduser(getUserID());
                    et_title.setText("");
                    et_description.setText("");

                    Call<Notes> call = ApiFastNotes.instance().createNote(tempNotes);
                    call.enqueue(new Callback<Notes>() {
                        @Override
                        public void onResponse(Call<Notes> call, Response<Notes> response) {
                            waitingCreateResponse=false;
                            finish();
                        }

                        @Override
                        public void onFailure(Call<Notes> call, Throwable t) {
                            Snackbar.make(mConstraintLayout, mConstraintLayout.getContext().getString(R.string.no_internet_error), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            waitingCreateResponse=false;
                        }
                    });
                }
            }
        });
    }

    private void initView(){
        et_title = (EditText) findViewById(R.id.et_title);
        et_description = (EditText) findViewById(R.id.et_description);
        btn_save = (Button) findViewById(R.id.btn_save);
        mConstraintLayout = (ConstraintLayout) findViewById(R.id.constraintCreateNote);
    }

    @Override
    public boolean  onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id==android.R.id.home) {
            finish();
        }
        return true;
    }

    private int getUserID() {
        return Remember.getInt(User.USER_ID, -7);
    }
}
