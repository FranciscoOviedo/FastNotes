package uca.fastnotes.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tumblr.remember.Remember;

import uca.fastnotes.api.models.User;
import uca.fastnotes.ui.login.LoginUser;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getUserID()==-7){
            Intent intent = new Intent(this, LoginUser.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, TabsView.class);
            startActivity(intent);
            finish();
        }
    }

    private int getUserID() {
        return Remember.getInt(User.USER_ID, -7);
    }
}
