package uca.fastnotes.ui.adapters;

public interface ItemClickListener <T> {
    void OnClick(T item);
}
