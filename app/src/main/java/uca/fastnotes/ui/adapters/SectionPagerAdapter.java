package uca.fastnotes.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import uca.fastnotes.ui.Fragments.AssignmentsFragment;
import uca.fastnotes.ui.Fragments.NotesFragment;

public class SectionPagerAdapter extends FragmentPagerAdapter {
    private AssignmentsFragment mFragmentsAssignments = new AssignmentsFragment();
    private NotesFragment mFragmetsNotes = new NotesFragment();
    public SectionPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return mFragmentsAssignments;
            case 1:
                return mFragmetsNotes;
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

}
