package uca.fastnotes.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uca.fastnotes.R;
import uca.fastnotes.api.ApiFastNotes;
import uca.fastnotes.api.models.Assignments;
import uca.fastnotes.ui.assignments.EditAssignmentActivity;

public class AssignmentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Assignments> mListItems;

    public AssignmentsAdapter(List<Assignments> items) {
        mListItems = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_assignment, parent, false);
        return new AssignmentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AssignmentViewHolder viewHolder = (AssignmentViewHolder) holder;
        viewHolder.setItem(mListItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }






    public class AssignmentViewHolder extends RecyclerView.ViewHolder {
        private Assignments mItem;
        private Context mContext;

        private CheckBox cb_state;
        private TextView tv_name;

        private ItemClickListener<Assignments> mClickListener;

        public AssignmentViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();

            cb_state = (CheckBox) itemView.findViewById(R.id.cb_state);
            tv_name = (TextView) itemView.findViewById(R.id.tv_title);

            tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(mContext, EditAssignmentActivity.class);
                    i.putExtra("AssignmentID", mItem.getIdassignment());
                    i.putExtra("AssignmentName", mItem.getName());
                    i.putExtra("AssignmentState", mItem.isState());
                    mContext.startActivity(i);
                }
            });

            tv_name.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    new MaterialDialog.Builder(view.getContext())
                            .title(R.string.delete_dialog_title)
                            .content(R.string.delete_dialog_content)
                            .positiveText(R.string.delete_dialog_action_positive)
                            .negativeText(R.string.delete_dialog_action_negative)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    Call<Assignments> call = ApiFastNotes.instance().deleteAssignment(String.valueOf(mItem.getIdassignment()));
                                    call.enqueue(new Callback<Assignments>() {
                                        @Override
                                        public void onResponse(Call<Assignments> call, Response<Assignments> response) {
                                            Realm realm = Realm.getDefaultInstance();
                                            realm.beginTransaction();
                                            int position = mListItems.indexOf(mItem);
                                            mListItems.get(position).deleteFromRealm();
                                            mListItems.remove(position);
                                            realm.commitTransaction();

                                            notifyItemRemoved(position);
                                        }

                                        @Override
                                        public void onFailure(Call<Assignments> call, Throwable t) {

                                        }
                                    });
                                }
                            })
                            .show();
                    return true;
                }
            });

            cb_state.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Assignments tempApiAssignment = new Assignments();
                    tempApiAssignment.setIdassignment(mItem.getIdassignment());
                    tempApiAssignment.setName(mItem.getName());
                    tempApiAssignment.setState(cb_state.isChecked());

                    Call<Assignments> call = ApiFastNotes.instance().updateAssignment(String.valueOf(tempApiAssignment.getIdassignment()), tempApiAssignment);
                    call.enqueue(new Callback<Assignments>() {
                        @Override
                        public void onResponse(Call<Assignments> call, Response<Assignments> response) {
                            Realm realm = Realm.getDefaultInstance();
                            Assignments results = realm.where(Assignments.class).equalTo("idassignment", mItem.getIdassignment()).findFirst();
                            realm.beginTransaction();
                            results.setState(cb_state.isChecked());
                            realm.commitTransaction();

                            if(cb_state.isChecked()){
                                tv_name.setPaintFlags(tv_name.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                            }else {
                                tv_name.setPaintFlags(tv_name.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                            }
                        }

                        @Override
                        public void onFailure(Call<Assignments> call, Throwable t) {
                            if(cb_state.isChecked()){
                                cb_state.setChecked(false);
                            }else {
                                cb_state.setChecked(true);
                            }
                        }
                    });
                }
            });
        }

        public void setItem(Assignments item){
            mItem = item;
            cb_state.setChecked(item.isState());
            tv_name.setText(item.getName());
            if(item.isState()){
                tv_name.setPaintFlags(tv_name.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                tv_name.setPaintFlags(tv_name.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            }
        }
    }
}
