package uca.fastnotes.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uca.fastnotes.R;
import uca.fastnotes.api.ApiFastNotes;
import uca.fastnotes.api.models.Notes;
import uca.fastnotes.ui.notes.EditNotesActivity;

public class NotesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Notes> mListItems;

    public NotesAdapter(List<Notes> items) {
        mListItems = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false);
        return new NotesAdapter.NotesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NotesAdapter.NotesViewHolder viewHolder = (NotesAdapter.NotesViewHolder) holder;
        viewHolder.setItem(mListItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }






    public class NotesViewHolder extends RecyclerView.ViewHolder {
        private Notes mItem;
        private Context mContext;

        private TextView tv_title;
        private TextView tv_description;
        private CardView cardViewNote;

        private ItemClickListener<Notes> mClickListener;

        public NotesViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_description = (TextView) itemView.findViewById(R.id.tv_description);
            cardViewNote = (CardView) itemView.findViewById(R.id.cardViewNote);

            cardViewNote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(mContext, EditNotesActivity.class);
                    i.putExtra("NotesID", mItem.getIdnotes());
                    i.putExtra("NotesTitle", mItem.getTitle());
                    i.putExtra("NotesDescription", mItem.getDescription());
                    mContext.startActivity(i);
                }
            });

            cardViewNote.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    new MaterialDialog.Builder(view.getContext())
                            .title(R.string.delete_dialog_title)
                            .content(R.string.delete_dialog_content)
                            .positiveText(R.string.delete_dialog_action_positive)
                            .negativeText(R.string.delete_dialog_action_negative)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    Call<Notes> call = ApiFastNotes.instance().deleteNote(String.valueOf(mItem.getIdnotes()));
                                    call.enqueue(new Callback<Notes>() {
                                        @Override
                                        public void onResponse(Call<Notes> call, Response<Notes> response) {
                                            Realm realm = Realm.getDefaultInstance();
                                            realm.beginTransaction();
                                            int position = mListItems.indexOf(mItem);
                                            mListItems.get(position).deleteFromRealm();
                                            mListItems.remove(position);
                                            realm.commitTransaction();

                                            notifyItemRemoved(position);
                                        }

                                        @Override
                                        public void onFailure(Call<Notes> call, Throwable t) {

                                        }
                                    });
                                }
                            })
                            .show();
                    return true;
                }
            });
        }

        public void setItem(Notes item){
            mItem = item;
            tv_title.setText(item.getTitle());
            tv_description.setText(item.getDescription());
        }
    }
}
