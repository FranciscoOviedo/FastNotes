package uca.fastnotes.ui.assignments;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uca.fastnotes.R;
import uca.fastnotes.api.ApiFastNotes;
import uca.fastnotes.api.models.Assignments;

public class EditAssignmentActivity extends AppCompatActivity {
    EditText et_name;
    Button btn_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_assignment);
        initView();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initView(){
        et_name = (EditText) findViewById(R.id.et_title);
        btn_edit = (Button) findViewById(R.id.btn_edit);
        Intent intent = getIntent();
        final int assignmentID = intent.getIntExtra("AssignmentID", 0);
        String name = intent.getStringExtra("AssignmentName");
        final boolean state = intent.getBooleanExtra("AssignmentState", false);
        et_name.setText(name);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Assignments tempAssignment = new Assignments();
                tempAssignment.setName(et_name.getText().toString());
                tempAssignment.setState(state);
                Call<Assignments> call = ApiFastNotes.instance().updateAssignment(String.valueOf(assignmentID), tempAssignment);
                call.enqueue(new Callback<Assignments>() {
                    @Override
                    public void onResponse(Call<Assignments> call, Response<Assignments> response) {
                        finish();
                    }

                    @Override
                    public void onFailure(Call<Assignments> call, Throwable t) {

                    }
                });
            }
        });
    }

    @Override
    public boolean  onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id==android.R.id.home) {
            finish();
        }
        return true;
    }
}
