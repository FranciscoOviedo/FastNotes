package uca.fastnotes.ui.assignments;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tumblr.remember.Remember;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uca.fastnotes.R;
import uca.fastnotes.api.ApiFastNotes;
import uca.fastnotes.api.models.Assignments;
import uca.fastnotes.api.models.User;

public class CreateAssignmentActivity extends AppCompatActivity {
    EditText et_name;
    Button btn_save;
    private ConstraintLayout mConstraintLayout;
    private boolean waitingCreateResponse = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_assignment);
        initView();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!waitingCreateResponse) {
                    waitingCreateResponse=true;
                    Assignments tempAssignment = new Assignments();
                    tempAssignment.setName(et_name.getText().toString());
                    tempAssignment.setState(false);
                    tempAssignment.setIduser(getUserID());
                    et_name.setText("");

                    Call<Assignments> call = ApiFastNotes.instance().createAssignment(tempAssignment);
                    call.enqueue(new Callback<Assignments>() {
                        @Override
                        public void onResponse(Call<Assignments> call, Response<Assignments> response) {
                            waitingCreateResponse=false;
                            finish();
                        }

                        @Override
                        public void onFailure(Call<Assignments> call, Throwable t) {
                            Snackbar.make(mConstraintLayout, mConstraintLayout.getContext().getString(R.string.no_internet_error), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            waitingCreateResponse=false;
                        }
                    });
                }
            }
        });
    }

    private void initView(){
        et_name = (EditText) findViewById(R.id.et_title);
        btn_save = (Button) findViewById(R.id.btn_save);
        mConstraintLayout = (ConstraintLayout) findViewById(R.id.constraintCreateAssignment);
    }

    @Override
    public boolean  onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id==android.R.id.home) {
            finish();
        }
        return true;
    }

    private int getUserID() {
        return Remember.getInt(User.USER_ID, -7);
    }
}
