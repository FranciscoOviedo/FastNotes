package uca.fastnotes;

import com.tumblr.remember.Remember;

import io.realm.Realm;

public class MyApplication extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        Remember.init(getApplicationContext(), "FastNotes.sync");
    }
}
